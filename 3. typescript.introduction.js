"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//  let const
var a = 1;
var b = 3;
// show case let in for
for (var i = 0; i < 100; i++) {
    var c = i;
    var b_1 = i; // this works.. ?
}
// types
// primitives
var myAwesomevar = "wqwe";
var myAwesomeNumber = 1;
var myAwesomeBoolean = true;
// arrays
var myAwesomeArray = [1, 2, 3, 4, 5];
var myOtherAwesomeArray = [true, false];
var x = myOtherAwesomeArray[0], y = myOtherAwesomeArray[1], rest = myOtherAwesomeArray.slice(2); // array destructuring
for (var a_1 in myAwesomeArray) {
    console.log("in: ", a_1);
}
for (var _i = 0, myAwesomeArray_1 = myAwesomeArray; _i < myAwesomeArray_1.length; _i++) {
    var a_2 = myAwesomeArray_1[_i];
    console.log("of: ", a_2);
}
function randomize(n) {
    if (n === void 0) { n = 0; }
    return Math.random() + n;
}
var randomix = function (n) {
    if (n === void 0) { n = 0; }
    return Math.random() + n;
};
var TaskType;
(function (TaskType) {
    TaskType["CONS"] = "CONS";
    TaskType["DEV"] = "DEV";
    TaskType["ARCH"] = "ARCH";
})(TaskType || (TaskType = {}));
var monday = {
    period: new Date(),
    taskType: TaskType.CONS,
    hours: 8,
    staffingId: "a",
    location: "onPremise"
};
function log(prefix) {
    return function (target, propertyKey, descriptor) {
        console.log(prefix, target);
        return descriptor;
    };
}
var Project = /** @class */ (function () {
    function Project(start, end, owner, title) {
        this.start = start;
        this.end = end;
        this.owner = owner;
        this.title = title;
        this.days = [];
    }
    Project.prototype.extendTo = function (newEnd) {
        this.end = newEnd;
    };
    Object.defineProperty(Project.prototype, "description", {
        get: function () {
            return this.title + " from " + this.start + " to " + this.end + " owned by " + this.owner;
        },
        enumerable: true,
        configurable: true
    });
    Project.prototype.bookStaffing = function (staffing) {
        var _a;
        var _this = this;
        var daysInProject = staffing.filter(function (s) {
            if (s.period instanceof Date) {
                return (_this.start.getTime() <= s.period.getTime() &&
                    s.period.getTime() <= _this.end.getTime());
            }
            else if (s.period instanceof Array) {
                return s.period.every(function (p) {
                    return _this.start.getTime() <= p.getTime() &&
                        p.getTime() <= _this.end.getTime();
                });
            }
        });
        (_a = this.days).push.apply(_a, daysInProject);
    };
    __decorate([
        log("critical"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Date]),
        __metadata("design:returntype", void 0)
    ], Project.prototype, "extendTo", null);
    return Project;
}());
new Project(new Date(), new Date(), "blabla", "solka").extendTo(new Date());
var Something = /** @class */ (function () {
    function Something() {
        this.name = "";
    }
    Something.prototype.doSomething = function (name) {
        this.name = name;
    };
    __decorate([
        log("doSomething"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], Something.prototype, "doSomething", null);
    return Something;
}());
new Something().doSomething("abcdd");
