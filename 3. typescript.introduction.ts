//  let const
let a = 1;

const b = 3;

// show case let in for
for (var i: number = 0; i < 100; i++) {
  let c = i;
  let b = i; // this works.. ?
}

// types
// primitives
var myAwesomevar: string = "wqwe";
var myAwesomeNumber: number = 1;
var myAwesomeBoolean: boolean = true;

// arrays
var myAwesomeArray: number[] = [1, 2, 3, 4, 5];
var myOtherAwesomeArray: Array<boolean> = [true, false];
var [x, y, ...rest] = myOtherAwesomeArray; // array destructuring

for (let a in myAwesomeArray) {
  console.log("in: ", a);
}

for (let a of myAwesomeArray) {
  console.log("of: ", a);
}

function randomize(n: number = 0) {
  return Math.random() + n;
}

const randomix = (n: number = 0) => Math.random() + n;

interface StaffingDay {
  period: Date[] | Date;
  taskType: TaskType;
  hours: number;
  staffingId: string;
  location: "remote" | "onPremise";
  shortText?: string;
}

enum TaskType {
  CONS = "CONS",
  DEV = "DEV",
  ARCH = "ARCH"
}

let monday: StaffingDay = {
  period: new Date(),
  taskType: TaskType.CONS,
  hours: 8,
  staffingId: "a",
  location: "onPremise"
};

function log(prefix: string) {
  return function(
    target: any,
    propertyKey: string,
    descriptor: TypedPropertyDescriptor<any>
  ): any {
    console.log(prefix, target);
    return descriptor;
  };
}

class Project {
  private days: StaffingDay[] = [];
  constructor(
    private start: Date,
    private end: Date,
    public owner: string,
    public title: string
  ) {}

  @log("critical")
  public extendTo(newEnd: Date) {
    this.end = newEnd;
  }

  public get description() {
    return `${this.title} from ${this.start} to ${this.end} owned by ${this.owner}`;
  }

  public bookStaffing(staffing: StaffingDay[]) {
    const daysInProject = staffing.filter(s => {
      if (s.period instanceof Date) {
        return (
          this.start.getTime() <= s.period.getTime() &&
          s.period.getTime() <= this.end.getTime()
        );
      } else if (s.period instanceof Array) {
        return s.period.every(
          p =>
            this.start.getTime() <= p.getTime() &&
            p.getTime() <= this.end.getTime()
        );
      }
    });
    this.days.push(...daysInProject);
  }
}

new Project(new Date(), new Date(), "blabla", "solka").extendTo(new Date());

class Something {
  public name: string = "";
  constructor() {}

  @log("doSomething")
  public doSomething(name: string) {
    this.name = name;
  }
}
new Something().doSomething("abcdd");
